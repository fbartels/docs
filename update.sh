#!/bin/bash

set -eu

echo "Installing mkdocs"
sudo pip3 install mkdocs==1.0.4

echo "Installing mkdocs-material"
sudo pip3 install mkdocs-material==3.0.3

echo "Installing pygments"
sudo pip3 install pygments

echo "Installing redoc-cli if needed"
if ! type redoc-cli &> /dev/null; then
    npm i -g redoc-cli
fi

echo "Done."
