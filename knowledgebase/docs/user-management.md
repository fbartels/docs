# User management

## Single Sign-On

Single Sign-on (SSO) is a feature of Cloudron where users can use the same credentials
(username & password) for logging in to apps.

SSO integration is optional and can be selected at the time of app installation. Turning off SSO
can be beneficial when the app is meant to be used primarily by external users (for example, a community
chat or a public forum).

Note that some apps do not support Cloudron SSO. Such apps manage their user credentials on their
own and users have to be managed inside the app itself.

When SSO is available for an app, the user management options will look like below:

<center>
<img src="/documentation/img/sso-available.png" class="shadow" width="500px">
</center>

When `Leave user management to the app` is selected, the app's Cloudron SSO integration will be disabled
and all user management has to be carried from within the app. This is useful when the app primarily caters
to external users (like say a community chat).

When SSO integration is unavailable for an app, the user management options look like below:

<center>
<img src="/documentation/img/sso-unavailable.png" class="shadow" width="500px">
</center>

## Users

New users can be added to the Cloudron with their email address from the `Users` menu.

<center>
<img src="/documentation/img/user-list.png" class="shadow" width="500px">
</center>

Click on `New User` to add a new user:

<center>
<img src="/documentation/img/users-add.png" class="shadow" width="500px">
</center>

They will receive an invite to sign up. Once signed up, they can access the apps they have been given access to.

To remove a user, simply remove them from the list. Note that the removed user cannot access any app anymore.

## Groups

Groups provide a convenient way to group users. You can assign one or more groups to apps to restrict who can
access for an app.

You can create a group by using the `Groups` menu item.

<center>
<img src="/documentation/img/users-groups-list.png" class="shadow">
</center>

Click on `New Group` to add a new group:

<center>
<img src="/documentation/img/users-groups-add.png" class="shadow">
</center>


To set the access restriction use the app's configure dialog.

<center>
<img src="/documentation/img/app-configure-group-acl.png" class="shadow" width="500px">
</center>

## Roles

Roles provide a way to restrict the permissions of a user. You can assign a role from the `Users` page.

<center>
<img src="/documentation/img/users-role.png" class="shadow" width="500px">
</center>

### User

A Cloudron user can login to the Cloudron dashboard and use the apps that they have access to.
They can edit their profile (name, password, avatar) on the dashboard.

### User Manager

A User Manager can add, edit and remove users & groups. Newly added users always get the `User` role.
User Manager cannot modify the role of an existing user.

### Administrator

A Cloudron administrator can manage apps and users. Note that an admin can login to any app even
if they have not been explicitly granted access in the `Access Control` section.

### Owner

A Cloudron owner has the capabilities of administrator with the addition of the ability to manage the
Cloudron subscription, manage backup settings and configure branding.

A good way to think about the owner role is a person who is in charge of server administration and billing.

!!! note "Automatic login"
    When clicking the `Manage Subscription` button in the `Settings` view, they are automatically logged
    in to the cloudron.io account.

## Password reset

### Users

The password reset mechanism relies on email delivery working reliably. Users can reset their own passwords.

In the event that [email delivery is not working](email/#debugging-mail-delivery), an administrator
can generate a new password reset link for another user by clicking on the 'Send invitation email' button.

<center>
<img src="/documentation/img/reinvite.png" class="shadow" width="500px">
</center>

This will open up a dialog showing the password reset link. If email delivery is not working for some
reason, the link can be sent to the user by some other means.

<center>
<img src="/documentation/img/invitation-dialog.png" class="shadow" width="500px">
</center>

### Admins

The password reset mechanism relies on email delivery working reliably. Admins can reset their own passwords
by navigating to `https://my.example.com/api/v1/session/password/resetRequest.html`.

In the event that [email delivery is not working](email/#debugging-mail-delivery), the reset token
can be determined by SSHing into the server:

```
mysql -uroot -ppassword -e "select username, email, resetToken from box.users";
```

Use the reset token displayed above to navigate to
`https://my.example.com/api/v1/session/password/reset.html?reset_token=<token>&email=<email>`

## Disable 2FA

If a user loses their 2FA device, the Cloudron administrator can disable the user's
2FA setup by SSHing into the server and running the following command:

```
# replace fred below with the actual username
root@my:~# mysql -uroot -ppassword -e "UPDATE box.users SET twoFactorAuthenticationEnabled=false WHERE username='fred'"
mysql: [Warning] Using a password on the command line interface can be insecure.
```

Once disabled, user can login with just their password. After login, they can
re-setup 2FA.

## Disable user

To disable a user, uncheck the `User is active` option. Doing so, will invalidate all
existing Cloudron dashboard session of the user and will log them out. The user may
still be able to use any apps that they were logged into prior to the de-activation.
To log them out from the apps, you can check if the app provides a way to log them out
(support for this depends on the app).

<center>
<img src="/documentation/img/user-disable.png" class="shadow" width="500px">
</center>

!!! note "Disabling does not delete user data"
    Disable a user only blocks the login access for the user. Any data generated by the
    user inside apps is not deleted.

## Impersonate user

One can create a file named `/home/yellowtent/platformdata/cloudron_ghost.json` which contains an username
and a fake password like:

```
{"girish":"secret123"}
```

With such a file in place, you can login to the Webadmin UI using the above credentials
(the user has to already exist). This helps you debug and also look into how the UI might
look from that user's point of view.

This file is intentionally located in `/tmp` for the off chance
that the system admin forgets about it (so it will get cleaned up on next reboot atleast).

## Valid usernames

The following characters are allowed in usernames:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

Usernames must be chosen with care to accomodate the wide variety of apps that run on Cloudron.
For example, very generic words like `error`, `pull`, `404` might be reserved by apps.

## External LDAP

The LDAP connector allows users from your existing LDAP directory to authenticate with Cloudron.
Each user account from the external directory will be automatically created on Cloudron and kept up-to-date.

For the moment, user synchronisation has to be **manually** triggered from within the dashboard using the
**Synchronize** button. A future release will synchronize users periodically.

The External LDAP sever configuration is set in the `Users` view.

<center>
<img src="/documentation/img/external-ldap-overview.png" class="shadow" width="500px">
</center>

### JumpCloud

The following screenshot shows the available configure options using a jumpcloud external LDAP directory:

* `Server URL`: `ldaps://ldap.jumpcloud.com:636`
* `Base DN`: `ou=users, o=3214565, dc=jumpcloud, dc=com`
* `Filter`: `(objectClass=inetorgperson)`
* `Bind DN`: `uid=ldap_admin,ou=Users,o=3214565,dc=jumpcloud,dc=com`
* `Bind password`: `admin password`
* `Username field`: `uid`

### Okta

To use the Okta integration, do the following:

* In Okta, enable the [LDAP interface](https://help.okta.com/en/prod/Content/Topics/Directory/LDAP_Using_the_LDAP_Interface.htm).
  You can do this from the `Directory Integrations` page.

* By default, Okta uses email as the default uid. Cloudron requires usernames for LDAP integration to work. If you already have a
  field in Okta that can provide usernames, provide that as the `username field`. If not, you can create a new field in the
  profile editor and set that.

* Cloudron configuration (replace 'org' below):
    * `Server URL`: `ldaps://<org>.ldap.okta.com`
    * `Base DN`: `ou=users, dc=<org>, dc=okta, dc=com`
    * `Filter`: `(objectClass=inetorgperson)`
    * `Bind DN`: `uid=<admin>, dc=<org>, dc=okta, dc=com`
    * `Bind password`: `admin password`
    * `Username field`: see above

<center>
<img src="/documentation/img/external-ldap-configuration.png" class="shadow" width="500px">
</center>


