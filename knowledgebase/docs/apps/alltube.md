# <img src="/documentation/img/alltube-logo.png" width="25px"> Alltube App

## Customization

Use the [Web terminal](/documentation/apps#web-terminal)
to edit custom configuration under `/app/data/config.yml`.

