# <img src="/documentation/img/bitwardenrs-logo.png" width="25px"> Bitwarden_rs App

This is the Rust implementation backend, *not* the official server backend, but fully compatible with the Client apps.

## Users

Bitwarden does not support Single Sign On. This is by design for security reasons. You must create a new password for your Bitwarden account.

By default it will allow open registration. This can be changed via environment variables at `/app/data/config.env`.
Uncomment the following lines to disable user signup and enable invite only setup:

```
export SIGNUPS_ALLOWED=false
export INVITATIONS_ALLOWED=true
```

## YubiKey

Bitwarden_rs has support for [YubiKeys](https://www.yubico.com/). To make use of them, just specify your client id and secret key in the `/app/data/config.env` and restart the app.

```
export YUBICO_CLIENT_ID="clientId"
export YUBICO_SECRET_KEY="secretKey"
```
