# <img src="/documentation/img/codimd-logo.png" width="25px"> CodiMD App

## Custom configuration

Use the [Web terminal](/documentation/apps#web-terminal)
to place custom configuration under `/app/data/config.json`.

See [CodiMD docs](https://github.com/hackmdio/codimd/wiki/Configuration-Files-and-Application-Settings)
for configuration options reference.

## Image uploads

By default, images are uploaded to the data directory of the app itself.
To switch to another provider like MinIO, first [configure minio](https://github.com/codimd/server/blob/master/docs/guides/minio-image-upload.md) to image uploads. Then, use the following configuration in
`/app/data/config.json`:

```
    "imageUploadType": "minio",
    "s3bucket": "codimd-images",
    "minio": {
        "accessKey": "MINIO_ACCESS_KEY",
        "secretKey": "MINIO_SECRET_KEY",
        "endPoint": "minio.cloudrondomain.com",
        "secure": true,
        "port": 443
    }
```

