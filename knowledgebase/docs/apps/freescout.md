# <img src="/documentation/img/freescout-logo.png" width="25px"> FreeScout App

## Mailbox Setup

Mailboxes do not need to be hosted on Cloudron itself. The app acts as a regular email client and thus can be setup for any IMAP mailbox.
For sending emails of a specific mailbox, the STMP method has to be selected as `php mail()` or `sendmail` wont work on Cloudron.

<img src="/documentation/img/freescout-smtp-settings.png" class="shadow">

## Migration

### Export data from old instance

Get a mysql database dump with the following command:
```
mysqldump -hmyservername -umyusername -pmypassword --single-transaction --routines --triggers databasename > mysqldump.sql
```
or if the app is already installed on a Cloudron:
```
mysqldump -h${CLOUDRON_MYSQL_HOST} -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} --single-transaction --routines --triggers ${CLOUDRON_MYSQL_USERNAME} > /tmp/mysqldump.sql
```

Download the resulting `mysqldump.sql` file to your laptop.

Then, to migrate file attachments, download the whole `storage` folder from within your FreeScout instance, next to the `mysqldump.sql` file.

### Install a new FreeScout instance

On your Cloudron install a fresh FreeScout instance and after installation, directly put the app into recovery mode in the *Repair* section of the app configuration page in the Cloudron dashboard. This will make sure the app is not actively running and thus using the database during import.

### Import data into new instance

Open a webterminal into the app from the *Console* view of the app configuration page. Use the terminal's upload button to upload the `mysqldump.sql` and import it into the database with:
```
mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /tmp/mysqldump.sql
```

Now upload the `storage` folder which includes all attachment files.
Either using the [Cloudron CLI](/documentation/custom-apps/cli/) tool:
```
cloudron push --app <newappdomain/appid> ./storage /app/data/
```
or zipping the `storage` folder, uploading it via the webterminal and extracting it to `/app/data/storage/`.

Finally disable the recovery mode from the Cloudron dashboard again to start the app proper with the new data.
