# <img src="/documentation/img/geoip-logo.png" width="25px"> GeoIP App

## About

A simple Geolocation service based on [maxmind](https://www.npmjs.com/package/maxmind).
  
## Usage

When making any request to either `/json` or `/jsonp?callback=functionName`, the service will
resolve the IP to a location and return as much information as possible about the country and city.

Additionally both routes can take a query param `ip` which will override the source ip.

Example:

```
$ curl https://geoip.example.com/json
{"city":{"geoname_id":5344157,"names":{"en":"Dublin","ru":"Дублин","zh-CN":"都伯林"}},"continent":{"code":"NA","geoname_id":6255149,"names":{"de":"Nordamerika","en":"North America","es":"Norteamérica","fr":"Amérique du Nord","ja":"北アメリカ","pt-BR":"América do Norte","ru":"Северная Америка","zh-CN":"北美洲"}},"country":{"geoname_id":6252001,"iso_code":"US","names":{"de":"USA","en":"United States","es":"Estados Unidos","fr":"États-Unis","ja":"アメリカ合衆国","pt-BR":"Estados Unidos","ru":"США","zh-CN":"美国"}},"location":{"accuracy_radius":50,"latitude":37.7201,"longitude":-121.919,"metro_code":807,"time_zone":"America/Los_Angeles"},"postal":{"code":"94568"},"registered_country":{"geoname_id":6252001,"iso_code":"US","names":{"de":"USA","en":"United States","es":"Estados Unidos","fr":"États-Unis","ja":"アメリカ合衆国","pt-BR":"Estados Unidos","ru":"США","zh-CN":"美国"}},"subdivisions":[{"geoname_id":5332921,"iso_code":"CA","names":{"de":"Kalifornien","en":"California","es":"California","fr":"Californie","ja":"カリフォルニア州","pt-BR":"Califórnia","ru":"Калифорния","zh-CN":"加利福尼亚州"}}]}
```

