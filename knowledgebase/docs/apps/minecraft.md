# <img src="/documentation/img/minecraft-logo.png" width="25px"> Minecraft App

# Supported editions

The Cloudron Minecraft package supports [Minecraft Java Edition Server](https://minecraft.gamepedia.com/Java_Edition)
as well as the [Bedrock/Pocket Edition](https://minecraft.gamepedia.com/Bedrock_Edition)

# Common server commands for Java Edition

Please note that you have to run these commands when the user is logged into the app instance from the Cloudron dashboard.
The username and password are your Cloudron credentials.

* Whitelist a client - `/whitelist minecraft_username`
* Blacklist a client - `/blacklist minecraft_username`
* Become the server operator - `/op your_minecraft_username`
* Reload server after chaning config files like **server.properties** - `/reload`
