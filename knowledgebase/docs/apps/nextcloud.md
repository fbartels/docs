# <img src="/documentation/img/nextcloud-logo.png" width="25px"> Nextcloud App

## Installing Nextcloud client on Ubuntu

Nextcloud provides its own desktop client for Linux in form of AppImage which can
be downloaded by issuing the following command:

```
sudo wget -nv https://download.nextcloud.com/desktop/releases/Linux/latest -O Nextcloud.AppImage
```

Alternatively, for Ubuntu, the latest version of the client can be installed from PPA following the instructions [here](https://launchpad.net/~nextcloud-devs/+archive/ubuntu/client):

```
sudo add-apt-repository ppa:nextcloud-devs/client
sudo apt-get update
sudo apt-get install nextcloud-client
```

For other platforms, please follow client installation instructions located at the [Nextcloud website](https://nextcloud.com/install/#install-clients).

## Plugin warning

We do not recommend installing apps in Nextcloud unless absolutely required. Maintaining such systems is a 
security hassle since you need to keep them up-to-date. Apps often break when Nextcloud is updated and you
have to know how to fix them. Finally, Nextcloud apps are not run sandboxed. This means that a faulty plugin
might compromise the whole app and also not make the app work at all. Nextcloud apps also write into the same
database as the main application which might result in unintended data corruption.

For the above reason, extensive use of Nextcloud plugins is highly discouraged since it will eventually break
your install.

## Running occ tool

The `occ` tool can be used for Nextcloud [administrative tasks](https://docs.nextcloud.com/server/12/admin_manual/configuration_server/occ_command.html).

The occ command can be run using the [Web terminal](/documentation/apps#web-terminal). For example, to list the users:

```
    sudo -u www-data php -f /app/code/occ user:list
```

## Resetting admin password

To reset the admin password, run the following occ command using the [Web terminal](/documentation/apps#web-terminal):

```
    sudo -u www-data php -f /app/code/occ user:resetpassword admin
```

## LDAP Sync

Nextcloud will periodically sync users from LDAP. However, we have noticed that this fails at times. To trigger a manual sync,
use the [Web terminal](/documentation/apps#web-terminal) and run the following command:


```
    sudo -u www-data php -f /app/code/occ ldap:check-user --update <username>
```

## Managing deleted files

When you delete a file in Nextcloud, it is not [immediately deleted](https://docs.nextcloud.com/server/12.0/user_manual/files/deleted_file_management.html) permanently. Instead, it is moved into the trash bin.
It is not permanently deleted until you manually delete it, or when the Deleted Files app deletes it to make room for
new files.

To configure, how items are permanently deleted, configure the [trashbin_retention_obligation](https://docs.nextcloud.com/server/12/admin_manual/configuration_server/config_sample_php_parameters.html#deleted-items-trash-bin) parameter.

The parameter can be edited using the [Web terminal](/documentation/apps#web-terminal) and editing the file
`/app/data/config/config.php`.

## Attaching external storage

Many VPS providers like Digital Ocean, Linode allow attaching external block storage to the server. Nextcloud has a feature
that allows mounting additional directories on the server as [external storage](https://docs.nextcloud.com/server/9/admin_manual/configuration_files/external_storage_configuration_gui.html).

Mounting an existing server directory as 'external storage' on Nextcloud is currently not supported.

If the intent is to simply increase the amount of storage available to Nextcloud (since you have run out of disk
space in the default data partition), there are two options:

* Configure Nextcloud to use an external object storage like Digital Ocean Spaces, AWS S3 etc
  * [DigitalOcean Spaces Guide](https://www.digitalocean.com/community/questions/is-it-possible-to-mount-do-spaces-as-external-storage-in-nextcloud-as-i-mount-aws-s3-storage)

* Configure Cloudron to store all of Nextcloud's data in the external block storage. To achieve this, follow the
  [guide](/documentation/storage/#moving-a-single-apps-data-directory-to-another-location) for
  moving a single app's data directory to another location.

Moving Nextcloud's directory entirely has the advantage that the iOS/Android app's Instant Upload feature uses
this new disk.

## Rescan files

Nextcloud will not pick up files if they are added directly in the data directory of the user on the server.
To make it rescan, open a [Web terminal](/documentation/apps#web-terminal) and run the following command:

```
sudo -u www-data php -f /app/code/occ files:scan <username>
```

To rescan external storage, use the `--path` parameter.

```
sudo -u www-data php -f /app/code/occ files:scan <username> --path=/<username>/files/externaltest
```

## Fixing a broken install

The [Nextcloud App Store](https://apps.nextcloud.com/) has a wide variety of apps that can be installed on
top of Nextcloud. Nextcloud has no native sandboxing mechanism for plugins - if a plugin fails, it will bring
down the whole installation. Plugins might also break an installation after a Nextcloud upgrade. For this reason,
we encourage carefully reviewing apps before using them.

To fix a broken installation, open a [Web terminal](/documentation/apps#web-terminal) and repair the app. Then run the following
commands:

```
sudo -u www-data php -f /app/code/occ app:list            # this lists the apps
sudo -u www-data php -f /app/code/occ app:disable <app>   # use this to disable the faulty app
sudo -u www-data php /app/code/occ maintenance:mode --off
```

After running the commands, end the repair for the app to come up.

## Collabora Online Document Editor

Collabora Online is a powerful online office suite that supports all major document, spreadsheet and presentation
file formats, which you can integrate in your own infrastructure. Key features are collaborative editing and
excellent office file format support.

See the [Collabora App docs](app/collabora) on how to setup Nextcloud with Collabora Office.

<img src="/documentation/img/nextcloud-collabora-editor.png" class="shadow">

## Previews

By default, Nextcloud generates previews for text and images. Previews for other document types is 
[disabled for privacy reasons](https://docs.nextcloud.com/server/18/admin_manual/configuration_server/config_sample_php_parameters.html#previews). Note that generating previews also require more memory and CPU.

To enable previews for PDF and OpenOffice documents, open a [Web terminal](/documentation/apps#web-terminal) and
edit `/app/data/config/config.php` and add the following setting:

```
'enabledPreviewProviders' =>
   array (
      0 => 'OC\\Preview\\TXT',
      1 => 'OC\\Preview\\MarkDown',
      2 => 'OC\\Preview\\OpenDocument',
      3 => 'OC\\Preview\\PDF',
      4 => 'OC\\Preview\\MSOffice2003',
      5 => 'OC\\Preview\\MSOfficeDoc',
      6 => 'OC\\Preview\\PDF',
      7 => 'OC\\Preview\\Image',
      8 => 'OC\\Preview\\Photoshop',
      9 => 'OC\\Preview\\TIFF',
     10 => 'OC\\Preview\\SVG',
     11 => 'OC\\Preview\\Font',
     12 => 'OC\\Preview\\MP3',
     13 => 'OC\\Preview\\Movie',
     14 => 'OC\\Preview\\MKV',
     15 => 'OC\\Preview\\MP4',
     16 => 'OC\\Preview\\AVI',
   ),
```

## Removing NextCloud users

To delete obsolete LDAP users and their data, see the [nextcloud docs](https://docs.nextcloud.com/server/stable/admin_manual/configuration_user/user_auth_ldap_cleanup.html?highlight=ldap).

## Email

NextCloud has apps like Rainloop to access email.

!!! warning "Not recommended"
    We do not recommend installing apps in Nextcloud unless absolutely required.
    Maintaining such systems is a security hassle since you need to keep them up-to-date.
    Apps often break when Nextcloud is updated and you have to know how to fix them.
    Finally, Apps are not run sandboxed. This means that a faulty plugin might compromise
    the whole app and also not make the app work at all.

That warning aside, it is possible to configure Rainloop to access mail as follows.

1. Login to Rainloop admin. You can find the admin link by going to Nextcloud `Settings` -> `Administration` -> `Additional Settings`.

2. Login as `admin`/`12345`

3. Add your domain for email. In the example below, we use `my.cloudron.cf`.

IMAP and SMTP configuration:

<center>
<img src="/documentation/img/nextcloud-rainloop-domain.png" class="shadow" width="500px">
</center>

Note that the SMTP encryption is turned off intentionally. This is safe because the communication
is within the same server. (STARTTLS is disabled by Cloudron Email intentionally in the internal
network for app compatibility when used with various languages and frameworks).

Sieve configuration:

<center>
<img src="/documentation/img/nextcloud-rainloop-sieve.png" class="shadow" width="500px">
</center>
