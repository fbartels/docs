# Import Redis

## Overview

In this guide, we will how to import redis database from your current setup into a Cloudron app.

Cloudron uses Redis' [RDB persistence](https://redis.io/topics/persistence) format for
point-in-time snapshots at specified intervals. To import the RDB, you must first
create a dump file in your existing redis installation. You can do this by
sending redis the `SAVE` command.

## Import

* Stop the app using the stop button in `Console` section. This will stop any dependent redis
container as well.

<center>
<img src="/documentation/guides/img/app-stop.png" class="shadow" width="500px">
</center>

* Identify, the app's id from the `Updates` section.

<center>
<img src="/documentation/guides/img/identify-appid.png" class="shadow" width="500px">
</center>

* Copy over the redis dump file via SCP. Be sure to replace `server-ip` with your server's IP
address and the app id `85a26cdf-3858-4784-b2c1-6ddb9c37e5b9` with your app's id below.

```
$ scp dump.rdb root@server-ip:/home/yellowtent/platformdata/redis/85a26cdf-3858-4784-b2c1-6ddb9c37e5b9/dump.rdb
dump.rdb                                                                                                  100%   692     4.8KB/s   00:00    
```

* Start the app using the start button in `Console`.

<center>
<img src="/documentation/guides/img/app-start.png" class="shadow" width="500px">
</center>

## Verify

* Open a [Web Terminal](/documentation/apps#web-terminal) by using the Terminal button in the `Console` section.

<center>
<img src="/documentation/guides/img/app-terminal.png" class="shadow" width="500px">
</center>

* Click the 'Redis' button on top of the terminal window. This will paste the Redis CLI command required to access redis.
  Simply, press enter to start using the Redis shell.

<center>
<img src="/documentation/guides/img/redis-shell.png" class="shadow" width="500px">
</center>

