# Networking

## IP Configuration

Using the IP configuration settings, you can configure the IP address, Cloudron uses
to configure to the DNS.

### Public IP

When using the `Public IP` provider, Cloudron will automatically detect the server's
public IP address by querying [this url](https://api.cloudron.io/api/v1/helper/public_ip).

<center>
<img src="/documentation/img/networking-public-ip.png" class="shadow" width="500px">
</center>

### Network Interface

If the server has multiple IP addresses, you can configure the preferred IP address by specifying
the network interface. The interfaces can be listed using `ip -f inet -br addr`.

<center>
<img src="/documentation/img/networking-interface-name.png" class="shadow" width="500px">
</center>

### Static IP

Use this option to provide a static IPv4 address. This IP address can be public or private.
Some use cases for using this provider are:

* [Digital Ocean Floating Address](https://www.digitalocean.com/docs/networking/floating-ips/)
* AWS VPC IP address

<center>
<img src="/documentation/img/networking-static-ip.png" class="shadow" width="500px">
</center>

## Private DNS

Cloudron uses the unbound DNS server internally to resolve all DNS queries
across all the apps.

If Cloudron is setup inside an internal network, you can customize the unbound
configuration to resolve internal hosts.

### Adding static hosts

To add some host entries internal to your network, add a file named `/etc/unbound/unbound.conf.d/custom.conf`:

```
server:

local-zone: "example.com." static
local-data: "jim.example.com. IN A 1.2.3.4"
local-data: "doug.example.com. IN A 2.3.4.5"
```

Be sure to restart the unbound service using `sudo systemctl restart unbound` and check it's status using
`sudo systemctl status unbound`.

### Internal DNS server

To add an internal DNS server, add a file named `/etc/unbound/unbound.conf.d/custom.conf`:

```
server:
    private-domain: "cloudron.lan"
    domain-insecure: "cloudron.lan"

forward-zone: 
    name: "cloudron.lan"
    forward-addr: 10.0.0.2
```

If your internal DNS server is not a forwarding server, use the `stub-zone:` and `stub-addr:` option instead.

Be sure to restart the unbound service using `sudo systemctl restart unbound` and check it's status using
`sudo systemctl status unbound`.

## Dynamic DNS

Enable this option to keep all your DNS records in sync with a changing IP address. This is useful when Cloudron
runs in a network with a frequently changing public IP address like a home connection.

<center>
<img src="/documentation/img/networking-dynamic-dns.png" class="shadow" width="500px">
</center>

