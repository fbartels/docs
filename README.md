This repository holds the Cloudron documentation, knowledge base and rest api docs

## Prerequisites

* mkdocs
* mkdocs-material
* redoc-cli

Just run ./update.sh it will install the dependencies

## Preview

```
cd knowledgebase
mkdocs serve
```

## Build

```
mkdocs Build
````

## API Docs

```
redoc-cli serve cloudron_api_swagger.yaml --disableGoogleFont=true --watch
```
